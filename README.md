Role Name
=========

Install mattermost Desktop client

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------


    - hosts: desktop
      roles:
         - dietrichliko/mattermost_client

License
-------

MIT

Author Information
------------------

Dietrich.Liko@oeaw.ac.at
